Для развертывания необходимо:
1) Добавить файл в `src/config/db_options.php` , где указать:

```
return [
    'host' => 'localhost',
    'user' => 'user_name',
    'password' => 'password',
    'dbname' => 'diplom'
];
```


2) Залить дамп базы данных 
3) Заменить алиас `'@domain'` в файле `src/config/db_options.php` на актуальный
