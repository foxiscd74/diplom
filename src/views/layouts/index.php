<?php
/**
 * @var string $title
 * @var string $content
 */

use services\kernel\Kernel as K;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="<?= K::getAlias('@domain') . '/src/web/style/style.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?= K::getAlias('@domain') . '/src/web/style/slider.css' ?>">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>

<header>
    <div class="container">
        <div class="row">
            <nav class="header">
                <ul class="header-menu">
                    <li class="header-menu__item">
                        <a href="/">
                            <img
                                    width="150px" height="100px"
                                    src="<?= K::getAlias('@domain') . K::getAlias('logo')  ?>"
                                    alt="Логотип"
                            >
                        </a>
                    </li>
                    <div class="header-menu__wrapper">
                        <li class="header-menu__item">
                            <input class="search" type="search">
                        </li>
                        <li class="header-menu__item col-sm-3">
                            <img width="40px" src="<?= K::getAlias('@domain') . '/src/web/img/svg/capa2.svg' ?>">
                        </li>
                        <li class="header-menu__item">
                            <?php if (empty($user = K::getKernel()->user()->getUser())): ?>
                                <a href="/register">
                                    <div class="btn btn-success">Регистрация</div>
                                </a>
                                <a href="/login">
                                    <div class="btn btn-primary">Войти</div>
                                </a>
                            <?php else: ?>
                                <p>
                                    Добро пожаловать,
                                    <a href="<?= K::getAlias('@domain') . '/profile/' . $user->getId() ?>">
                                        <?= $user->getNickname() ?>
                                    </a>
                                </p>
                                <a href="/logout">
                                    <div class="btn btn-primary">Выйти</div>
                                </a>
                            <?php endif; ?>
                        </li>
                    </div>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main class="container">
    <div class="row">
        <div class="col-sm-9">
            <h1><?= $title ?></h1>
        </div>
        <div class="col-sm-3">
            <?php if ($user = K::getKernel()->user()->getUser()): ?>
                <a href="/add_que">
                    <div class="btn btn-primary add-que">
                        Добавить объявление
                    </div>
                </a>
            <?php endif; ?>
        </div>
    </div>

    <p style="background-color: red; color: white; border-radius: 10px; text-align: center; line-height: 30px; font-size: 14px">
        <?= K::getKernel()->session()->getError('ErrorMessage'); ?>
    </p>
    <?= $content ?>
</main>
<footer>
    <div class="container">
        <div class="row footer_info">
            <div class="col-sm-4">
                <a href="/">
                    <img
                            width="150px" height="100px"
                            src="<?= K::getAlias('@domain') . K::getAlias('logo') ?>"
                            alt="Логотип"
                    >
                </a>
            </div>
            <div class="col-sm-4">
                <h4>Контакты</h4>
                <p>Тел: <a href="tel: +74951234567">+7 (123) 123-12-12</a></p>
                <p>Email: <a href="mailto: example@mail.ru">example@mail.ru</a></p>
            </div>
            <div class="col-sm-4">
                <p>All rights reserved © 2021</p>
            </div>
        </div>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossorigin="anonymous"></script>
<script src="<?= K::getAlias('@domain') . '/src/web/js/jquery.js ' ?>"></script>
<script src="<?= K::getAlias('@domain') . '/src/web/js/jquery_migrate.js ' ?>"></script>
<script src="<?= K::getAlias('@domain') . '/src/web/js/slider.js ' ?>"></script>
<script src="<?= K::getAlias('@domain') . '/src/web/js/js.js ' ?>"></script>
</body>
</html>