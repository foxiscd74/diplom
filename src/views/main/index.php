<?php
/**
 * @var Questionnaires[] $questionnaires
 * @var CarModels[] $carModels
 * @var int $count
 */

use services\kernel\Kernel as K;
use models\CarModels;
use models\Questionnaires;

$filterStr = '';
if (!empty($_GET['filter'])):
    $filterStr .= 'filter=' . $_GET['filter'] . '&';
    $filterStr .= 'from_date_issue=' . $_GET['from_date_issue'] . '&';
    $filterStr .= 'to_date_issue=' . $_GET['to_date_issue'] . '&';
    $filterStr .= 'from_price=' . $_GET['from_price'] . '&';
    $filterStr .= 'to_price=' . $_GET['to_price'] . '&';
    $filterStr .= 'from_HP=' . $_GET['from_HP'] . '&';
    $filterStr .= 'to_HP=' . $_GET['to_HP'] . '&';
    $filterStr .= 'model=' . $_GET['model'];
endif;
?>
<div class="row">
    <div class="col-12">
        <div class="item">
            <div class="row">
                <?php
                if (!empty($questionnaires)): ?>
                    <?php
                    foreach ($questionnaires as $questionnaire): ?>
                        <div class="card_item">
                            <div class="col-6">
                                <div class="card_photo">
                                    <div class="single-item">
                                        <?php
                                        if ($questionnaire->getImages()) : ?>
                                            <?php
                                            foreach ($questionnaire->getImages() as $image): ?>
                                                <div class="item-picture">
                                                    <img src="<?=
                                                    (K::getAlias('@domain')
                                                        . $image->getImage())
                                                    ?>"
                                                         alt="МАШИНА">
                                                </div>
                                            <?php
                                            endforeach; ?>
                                        <?php
                                        else: ?>
                                            <div class="item-picture">
                                                <img src="<?=
                                                K::getAlias('@domain')
                                                . K::getAlias('noImg')
                                                ?>"
                                                     alt="МАШИНА">
                                            </div>
                                        <?php
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="item-info">
                                    <span style="text-align: right; width: 100%"><?= $questionnaire->getCreatedAt(
                                        ) ?></span>
                                    <div class="item-info__title">
                                        <h4> Владелец:
                                            <a href="<?= K::getAlias('@domain') . '/profile/' .
                                            $questionnaire->getUser()->id ?>">
                                                <?= $questionnaire->getUser()->email ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="item-info__description">
                                        <?= $questionnaire->getDescription() ?>
                                    </div>
                                    <ul class="item-info__characteristics">
                                        <h4>Характеристики:</h4>
                                        <li>Л/с: <?= $questionnaire->getHP() ?></li>
                                        <li>Год выпуска: <?= $questionnaire->getDateOfIssue() ?></li>
                                        <li>Модель: <?= $questionnaire->getModel()->getModel() ?></li>
                                    </ul>
                                    <div class="item-info__price">
                                        <span><?= $questionnaire->getPrice() ?> р.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach; ?>
                    <div style="display: flex ; justify-content: center">
                        <?php
                        if (!$count['start']): ?>
                            ...
                        <?php
                        endif; ?>
                        <?php
                        foreach ($count['numbers'] as $number): ?>
                            <div style=" width: 20px; height: 20px; background-color: bisque; border-radius: 3px; margin: 3px; text-align: center">
                                <a href="?page=<?= $number ?>&<?= $filterStr ?>">
                                    <?= $number ?>
                                </a></div>
                        <?php
                        endforeach; ?>
                        <?php
                        if (!$count['end']): ?>
                            ...
                        <?php
                        endif; ?>
                    </div>
                <?php
                endif; ?>
            </div>
        </div>
    </div>
</div>
<!-- filter start -->
<div class="filter_menu">
    <div class="filter_close_name" onclick="toggleFilter(this)"><h3 class="filter_name">Ф И Л Ь Т Р</h3></div>
    <div class="container">
        <h3 class="filter_name">Фильтр</h3>
        <form action="" class="filter_form" method="get">
            <div class="filter_menu_item">
                <input type="hidden" name="filter" value="true">
                <div class="form-text">Дата выпуска</div>
                <input placeholder="От" id="from_date_issue" type="date" name="from_date_issue"
                       value="<?= !empty($_GET['from_date_issue']) ?
                           $_GET['from_date_issue'] :
                           '' ?>">
                <input placeholder="До" id="to_date_issue" type="date" name="to_date_issue"
                       value="<?= !empty($_GET['to_date_issue']) ?
                           $_GET['to_date_issue'] :
                           '' ?>">
            </div>
            <div class="filter_menu_item">
                <div class="form-text">Цена</div>
                <div class="flex">
                    <input placeholder="От" id="from_price" type="number" name="from_price"
                           value="<?= !empty($_GET['from_price']) ?
                               $_GET['from_price'] :
                               '' ?>">
                    <input placeholder="До" id="to_price" type="number" name="to_price"
                           value="<?= !empty($_GET['to_price']) ?
                               $_GET['to_price'] :
                               '' ?>">
                </div>
            </div>
            <div class="filter_menu_item">
                <div class="form-text">Лошадиные силы</div>
                <div class="flex">
                    <input placeholder="От" id="from_HP" type="number" name="from_HP"
                           value="<?= !empty($_GET['from_HP']) ?
                               $_GET['from_HP'] :
                               '' ?>">
                    <input placeholder="До" id="to_HP" type="number" name="to_HP"
                           value="<?= !empty($_GET['to_HP']) ?
                               $_GET['to_HP'] :
                               '' ?>">
                </div>
            </div>
            <div class="filter_menu_item">
                <label for="model">
                    <div class="form-text">Модель</div>
                </label>
                <select name="model" class="custom-select mr-sm-2">
                    <option value="0" selected>Марка...</option>
                    <?php
                    foreach ($carModels as $model): ?>
                        <option value="<?= $model->getId() ?>"><?= $model->getModel() ?></option>
                    <?php
                    endforeach; ?>
                </select>
            </div>
            <div class="btn" style=" padding-top: 8px; padding-left: 0px">
                <button type="submit" class="btn btn-primary" style="border: 1px solid black">Найти</button>
            </div>
            <div onclick="clearForm(this)" class="btn btn-primary" style="border: 1px solid black;margin-top: 3px">
                Отчистить
            </div>
        </form>
    </div>
</div>
<!-- filter end -->
