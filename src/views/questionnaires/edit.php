<?php

use services\kernel\Kernel as K;
use models\CarModels;
use models\forms\QueForm;
use models\Questionnaires;

/**
 * @var CarModels $carModels
 * @var QueForm $model
 * @var Questionnaires $questionnaire
 */

?>
<div class="row">
    <div class="col-12">
        <div class="item">
            <div class="row">
                <h3>Изображения</h3>
                <div class="mb-3" style="display: flex">
                    <?php
                    foreach ($questionnaire->getImages() as $image): ?>
                        <div class="item-picture" data-img="<?= $image->id ?>">
                            <img onclick="menuImg(this)"
                                 src="<?= K::getAlias('@domain') . $image->getImage() ?>"
                                 alt="МАШИНА">
                            <div onclick="deleteImage(this)" class="delete_button">
                                Удалить
                            </div>
                        </div>
                    <?php
                    endforeach; ?>
                </div>
                <form method="post" multipart="" enctype="multipart/form-data">
                    <div class="mb-3">
                        <input type="file" name="images[]" multiple>
                    </div>
                    <div class="mb-3">
                        <label class="mr-sm-2" for="inlineFormCustomSelect">Марка автомобиля</label>
                        <select name="modelId" class="custom-select mr-sm-2">
                            <option disabled>Марка...</option>
                            <?php
                            foreach ($carModels as $value): ?>
                                <option value="<?= $value->getId() ?>" <?= $model->modelId == $value->getId() ?
                                    'selected' :
                                    '' ?>><?= $value->getModel() ?></option>
                            <?php
                            endforeach; ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <textarea name="description" class="form-control"><?= $model->description ?></textarea>
                        <div id="emailHelp" class="form-text">Описание</div>
                    </div>
                    <div class="mb-3">
                        <input type="date" name="dateOfIssue" class="form-control"
                               value="<?= (new DateTime($model->dateOfIssue))->format('Y-m-d') ?>">
                        <div id="emailHelp" class="form-text">Дата выпуска</div>
                    </div>
                    <div class="mb-3">
                        <input type="number" name="hP" class="form-control" value="<?= $model->hP ?>">
                        <div id="emailHelp" class="form-text">Лошадиные силы</div>
                    </div>
                    <div class="mb-3">
                        <input type="number" name="price" class="form-control" value="<?= $model->price ?>">
                        <div id="emailHelp" class="form-text">Цена</div>
                    </div>
                    <button type="submit" class="btn btn-primary">Изменить</button>
                </form>
            </div>
        </div>
    </div>
</div>