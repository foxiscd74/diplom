<?php

use services\kernel\Kernel as K;
use models\CarModels;

/**
 * @var \models\CarModels[] $models
 */

?>
<div class="row">
    <div class="col-12">
        <div class="item">
            <div class="row">
                <form method="post"  multipart="" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label class="mr-sm-2" for="inlineFormCustomSelect">Марка автомобиля</label>
                        <select name="modelId" class="custom-select mr-sm-2">
                            <option selected disabled>Марка...</option>
                            <?php foreach ($models as $model): ?>
                                <option value="<?= $model->getId() ?>"><?= $model->getModel() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <input type="file" name="images[]" multiple>
                    </div>
                    <div class="mb-3">
                        <textarea name="description" class="form-control"></textarea>
                        <div id="emailHelp" class="form-text">Описание</div>
                    </div>
                    <div class="mb-3">
                        <input type="date" name="dateOfIssue" class="form-control">
                        <div id="emailHelp" class="form-text">Дата выпуска</div>
                    </div>
                    <div class="mb-3">
                        <input type="number" name="hP" class="form-control">
                        <div id="emailHelp" class="form-text">Лошадиные силы</div>
                    </div>
                    <div class="mb-3">
                        <input type="number" name="price" class="form-control">
                        <div id="emailHelp" class="form-text">Цена</div>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
        </div>
    </div>
</div>