<?php

use services\kernel\Kernel as K;

?>
<div class="row">
    <div class="col-12">
        <div class="item">
            <div class="row">
                <form method="post">
                    <div class="mb-3">
                        <input type="email" name="email" class="form-control">
                        <div id="emailHelp" class="form-text">Email</div>
                    </div>
                    <div class="mb-3">
                        <input type="text" name="nickname" class="form-control">
                        <div id="emailHelp" class="form-text">Login</div>
                    </div>
                    <div class="mb-3">
                        <input type="password" name="password" class="form-control">
                        <div id="emailHelp" class="form-text">Пароль</div>
                    </div>
                    <button type="submit" class="btn btn-primary">Зарегестрироваться</button>
                </form>
            </div>
        </div>
    </div>
</div>