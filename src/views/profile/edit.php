<?php
use services\kernel\Kernel as K;
use models\forms\ProfileForm;

/**
 * @var ProfileForm $model
 */

?>
<div class="row">
    <div class="col-12">
        <div class="item">
            <div class="row">
                <form method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <input type="text" name="email" class="form-control" value="<?= $model->email ?>">
                        <div class="form-text">Email</div>
                    </div>
                    <div class="mb-3">
                        <input type="text" name="firstName" class="form-control" value="<?= $model->firstName ?>">
                        <div class="form-text">Имя</div>
                    </div>
                    <div class="mb-3">
                        <input type="text" name="middleName" class="form-control" value="<?= $model->middleName ?>">
                        <div class="form-text">Отчество</div>
                    </div>
                    <div class="mb-3">
                        <input type="text" name="lastName" class="form-control" value="<?= $model->lastName ?>">
                        <div class="form-text">Фамилия</div>
                    </div>
                    <div class="mb-3">
                        <input type="date" name="birthDate" class="form-control" value="<?= $model->birthDate ?>">
                        <div class="form-text">Дата рождения</div>
                    </div>
                    <div class="mb-3">
                        <input type="tel" name="phone" class="form-control" value="<?= $model->phone ?>">
                        <div class="form-text">Телефон</div>
                    </div>
                    <div class="mb-3">
                        <input type="text" name="address" class="form-control" value="<?= $model->address ?>">
                        <div class="form-text">Адрес</div>
                    </div>
                    <div class="mb-3">
                        <input type="file" name="avatar" class="form-control" value="<?= $model->avatar ?>">
                        <div class="form-text">avatar</div>
                    </div>
                    <button type="submit" class="btn btn-primary">Изменить</button>
                </form>
            </div>
        </div>
    </div>
</div>