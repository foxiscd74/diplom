<?php
use services\kernel\Kernel as K;
use models\Profile;
use models\User;
use models\Questionnaires;

/**
 * @var Questionnaires[] $questionnaires
 * @var User $user
 * @var Profile $profile
 * @var bool $checkUser
 */

if (empty($userName = $profile->firstName . ' ' . $profile->lastName)) {
    $userName = $user->getNickname();
}
?>
<div class="row">
    <div class="col-6">
        <div class="profile_info_blocks">
            <h2 style="text-align: center">Информация о пользователе</h2>
            <div class="profile_img_block">
                <div class="img" style="background-image: url(<?= K::getAlias('@domain') . $profile->avatar?:K::getAlias('noImg') ?>)"></div>
            </div>
            <div class="profile_info">
                <div class="description">Email:</div>
                <div class="text"><?= $user->email ?></div>
            </div>
            <?php if ($profile->firstName): ?>
                <div class="profile_info">
                    <div class="description">Имя:</div>
                    <div class="text"><?= $profile->firstName ?></div>
                </div>
            <?php endif; ?>
            <?php if ($profile->lastName): ?>
                <div class="profile_info">
                    <div class="description">Фамилия:</div>
                    <div class="text"><?= $profile->lastName ?></div>
                </div>
            <?php endif; ?>
            <?php if ($profile->middleName): ?>
                <div class="profile_info">
                    <div class="description">Отчество:</div>
                    <div class="text"><?= $profile->middleName ?></div>
                </div>
            <?php endif; ?>
            <?php if ($profile->birthDate): ?>
                <div class="profile_info">
                    <div class="description">Дата рождения:</div>
                    <div class="text"><?= $profile->birthDate ?></div>
                </div>
            <?php endif; ?>
            <?php if ($profile->phone): ?>
                <div class="profile_info">
                    <div class="description">Телфеон:</div>
                    <div class="text"><?= $profile->phone ?></div>
                </div>
            <?php endif; ?>
            <?php if ($profile->address): ?>
                <div class="profile_info">
                    <div class="description">Адрес проживания:</div>
                    <div class="text"><?= $profile->address ?></div>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($checkUser): ?>
            <a href="/profile/<?= $user->getId() ?>/edit">
                <div class="btn btn-primary add-que">
                    Редактировать информацию
                </div>
            </a>
        <?php endif; ?>
    </div>
    <?php if (!empty($questionnaires)): ?>
        <div class="col-6">
            <h2 style="text-align: center">Объявления <?= $userName ?></h2>
            <div class="item">
                <div class="row">
                    <?php foreach ($questionnaires as $questionnaire): ?>
                        <div class="card_item">
                            <div class="col-6">
                                <div class="card_photo">
                                    <div class="single-item">
                                        <?php if ($questionnaire->getImages()) : ?>
                                            <?php foreach ($questionnaire->getImages() as $image): ?>
                                                <div class="item-picture">
                                                    <img
                                                            src="<?= K::getAlias('@domain') . $image->getImage() ?>"
                                                            alt="МАШИНА"
                                                    >
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="item-picture">
                                                <img src="<?= K::getAlias('@domain') . K::getAlias('noImg') ?>"
                                                     alt="МАШИНА">
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="item-info">
                                    <span style="text-align: right; width: 100%"><?= $questionnaire->getCreatedAt() ?></span>
                                    <div class="item-info__title">
                                        <h4> Владелец: <?= $questionnaire->getUser()->getNickname() ?></h4>
                                    </div>
                                    <div class="item-info__description">
                                        <?= $questionnaire->getDescription() ?>
                                    </div>
                                    <ul class="item-info__characteristics">
                                        <h4>Характеристики:</h4>
                                        <li>Л/с: <?= $questionnaire->getHP() ?></li>
                                        <li>Год выпуска: <?= $questionnaire->getDateOfIssue() ?></li>
                                        <li>Модель: <?= $questionnaire->getModel()->getModel() ?></li>
                                    </ul>
                                    <div class="item-info__price">
                                        <span><?= $questionnaire->getPrice() ?> р.</span>
                                    </div>
                                </div>
                            </div>
                            <?php if ($checkUser): ?>
                                <div class="after_card">
                                    <div onclick="redirect('/que/<?= $questionnaire->getId() ?>/edit')">Редактировать
                                    </div>
                                    <div onclick="checkActionDelete('/que/<?= $questionnaire->getId() ?>/delete')">
                                        Удалить
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>