<?php

namespace services;

class Pagination
{
    private $models;
    private $limit;
    private $number;

    /**
     * Pagination constructor.
     * @param array $models Массив данных
     * @param int $limit Количество выводимых элементов
     * @param null $number Номер страницы
     */
    public function __construct(array $models, int $limit = 5, $number = null)
    {
        $this->models = $models;
        $this->limit = $limit;
        $this->number = $number;
    }

    /**
     * Возвращает массив лимитированной информации
     * @return array
     */
    public function getPageContent()
    {
        $arr = [];
        $number = $this->number;
        if ($number !== null) {
            $number = $number - 1;
            $from = $number * $this->limit;
        } else {
            $from = 0;
        }
        $to = $from + $this->limit;
        for ($i = $from; $i < $to; $i++) {
            if (!empty($this->models[$i])) {
                $arr[] = $this->models[$i];
            }
        }
        return $arr;
    }

    /**
     * Возвращает массив количества страниц страниц
     * @param int $limitPage Лимит по вкладкам
     * @return array
     */
    public function getCountPage($limitPage = 4)
    {
        $count = [];
        $count['end'] = false;
        $count['start'] = false;
        $start = $this->number - floor($limitPage / 2);
        $end = $this->number + floor($limitPage / 2);

        if ($end >= ceil(count($this->models) / $this->limit)) {
            $end = ceil(count($this->models) / $this->limit);
            $start = ceil(count($this->models) / $this->limit) - $limitPage;
            $count['end'] = true;
        }
        if ($start <= 1) {
            $start = 1;
            $end = $start + $limitPage;
            $count['start'] = true;
        }
        if ($end > ceil(count($this->models) / $this->limit)) {
            $end = ceil(count($this->models) / $this->limit);
            $count['end'] = true;
        }
        for ($i = $start; $i <= $end; $i++) {
            if ($limitPage > 0) {
                $count['numbers'][] = $i;
                $limitPage--;
            }
        }
        return array_reverse($count);
    }
}
