<?php

namespace services\kernel;

/**
 * Interface KernelInterface
 * @package services\kernel
 */
interface SessionInterface
{
    /**
     * Добавить значение
     * @param string $name Название переменной
     * @param $value
     */
    public function setSession($name, $value);

    /**
     * Получить значение
     * @param string $name Название переменной
     * @return mixed
     */
    public function getSession($name);

    /**
     * Удалить значение
     * @param string $name Название переменной
     */
    public function deleteSession($name);

    /**
     * Получение ошибки с последующим удалением
     * @param string $name Название переменной
     * @return mixed
     */
    public function getError($name);
}