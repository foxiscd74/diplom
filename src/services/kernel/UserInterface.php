<?php

namespace services\kernel;

use models\User;

/**
 * Interface KernelInterface
 * @package services\kernel
 */
interface UserInterface
{
    /**
     * Выход пользователя
     * @return null
     */
    public function logout();

    /**
     * Авторизация пользователя
     * @param User $user Пользователь
     */
    public function login($user);

    /**
     * Получить пользователя по токену
     * @return User|null
     */
    public function getUser();
}