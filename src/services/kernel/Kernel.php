<?php

namespace services\kernel;

use ReflectionException;
use exceptions\KernelException;
use models\User;
use ReflectionClass;

/**
 * Class Kernel
 * Ядро проекта, с помощью него можно получить доступ до различных сервисов
 * 1) пример вызова операции:
 *      Kernel::getKernel()->getOperations()->testOperation($arguments);
 *      Для добавления новых операций необходимо:
 *          1) наследоваться от AbstractKernelOperation
 *          2) добавить компонент в настройки системы (config/local_config.php)
 * 2) Пример создания сесессий:
 *      Kernel::getKernel()->session()->setSession('name','value');
 * 3) Пример получения авторизованного пользователя:
 *      Kernel::getKernel()->user()->getUser();
 * @package services\kernel
 * @property string $component
 * @property KernelInterface $instance
 * @property object[] $operations_objects
 */
class Kernel implements KernelInterface, SessionInterface, UserInterface
{
    private static $instance;
    private $operations_objects = [];
    private $component = '';

    /**
     * Получить все операции
     * @return KernelInterface
     */
    public function getOperations(): KernelInterface
    {
        $this->component = 'getOperations';
        return self::$instance;
    }

    /**
     * Закрытый конструктор
     */
    protected function __construct()
    {
    }

    /**
     * Закрыто клонирование
     */
    protected function __clone()
    {
    }

    /**
     * Возвращает экземпляр ядра с доступом к внутренним компонентам
     * @return KernelInterface
     */
    public static function getKernel(): KernelInterface
    {
        if (self::$instance instanceof Kernel) {
            return self::$instance;
        } else {
            $kernel = self::$instance = new self();
            $kernel->run();
            return $kernel;
        }
    }

    /**
     * Возвращает значение алиасов
     * @param string $alias_name Название алиаса
     * @return mixed
     */
    public static function getAlias(string $alias_name)
    {
        $aliases = (include DIR_APP . 'config/local-config.php')['aliases'];
        foreach ($aliases as $name => $alias) {
            if ($name == $alias_name) {
                return $alias;
            }
        }
    }

    /**
     * Запуск компонентов ядра
     * @return KernelInterface
     */
    private function run(): KernelInterface
    {
        $kernel = self::$instance;
        $operations = (include DIR_APP . 'config/local-config.php')['operations'];
        foreach ($operations as $operation) {
            $kernel->operations_objects[] = new $operation;
        }
        return $kernel;
    }

    /**
     * @param string $name Название вызванного метода
     * @param mixed $arguments Аргументы вызываемого метода
     * @return mixed|void
     * @throws ReflectionException
     */
    public function __call(string $name, $arguments = null)
    {
        try {
            switch ($this->component) {
                case 'getOperations':
                    return $this->getOperationMethod($name, $arguments);
            }
        } catch (KernelException $exception) {
            exit($exception->getMessage());
        }
    }

    /**
     * Возвращает метод операции
     * @param string $name Название вызванного метода
     * @param array|null $arguments Аргументы вызываемого метода
     * @return mixed
     * @throws KernelException
     * @throws ReflectionException
     */
    private function getOperationMethod(string $name, $arguments)
    {
        foreach ($this->operations_objects as $operation) {
            if (method_exists($operation, $name)) {
                $obj = new ReflectionClass($operation);
                if ($obj->getMethod($name)->isPublic()) {
                    return $operation->$name(...$arguments);
                }
                throw new KernelException('Метод ' . $name . ' должен быть public');
            }
        }
        throw new KernelException('Метода ' . $name . ' не существует');
    }

    /**
     * Создание сессии
     * @return KernelInterface|SessionInterface
     */
    public function session()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        return self::$instance;
    }

    /**
     * Добавить значение
     * @param string $name Название переменной
     * @param mixed $value Значение
     */
    public function setSession($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * Получить значение
     * @param string $name Название переменной
     * @return mixed
     */
    public function getSession($name)
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return null;
    }

    /**
     * Удалить значение
     * @param string $name название
     */
    public function deleteSession($name)
    {
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * Получение ошибки с последующим удалением
     * @param string $name Название переменной
     * @return mixed
     */
    public function getError($name)
    {
        if (isset($_SESSION[$name])) {
            $message = $_SESSION[$name];
            unset($_SESSION[$name]);
            return $message;
        }
        return null;
    }

    /**
     * Получить пользователя
     * @return KernelInterface|UserInterface
     */
    public function user()
    {
        return self::$instance;
    }

    /**
     * Авторизация пользователя
     * @param User $user Пользователь
     */
    public function login($user)
    {
        self::$instance->getOperations()->createToken($user);
    }

    /**
     * Выход пользователя
     * @return null
     */
    public function logout()
    {
        self::$instance->getOperations()->deleteToken();
    }

    /**
     * Получить пользователя по токену
     * @return User|null
     */
    public function getUser()
    {
        return self::$instance->getOperations()->getUserByToken();
    }
}