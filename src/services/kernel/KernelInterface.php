<?php

namespace services\kernel;

/**
 * Interface KernelInterface
 * @package services\kernel
 */
interface KernelInterface
{
    /**
     * Возвращает все доступные операции ядра
     * @return KernelOperationInterface
     */
    public function getOperations();

    /**
     * Возвращает сессии
     * @return SessionInterface
     */
    public function session();

    /**
     * Метод для получения юзера
     * @return UserInterface
     */
    public function user();
}