<?php

namespace services\kernel;

use models\ImageInterface;
use models\User;

/**
 * Interface KernelOperationInterface
 * Интерфейс для отображения phpdoc операций ядра
 * @package services\kernel
 */
interface KernelOperationInterface
{
    /**
     * Создать токен
     * @param User $user Пользователь
     */
    public function createToken(User $user);

    /**
     * Удалить токен
     */
    public function deleteToken();

    /**
     * Получить пользователя по токену
     * @return User|null
     */
    public function getUserByToken();

    /**
     * Удалить файл
     * @param ImageInterface $model Модель
     */
    public function deleteFile(ImageInterface $model);

    /**
     * Изменить файл
     * @param array $file Файл
     * @param ImageInterface $model Модель
     */
    public function updateFile($file, ImageInterface $model);

    /**
     * Метод сохранения файл
     * @param array $file Файл
     * @param ImageInterface $model Модель
     */
    public function saveFile($file, ImageInterface $model);

    /**
     * Получить картинки в формате объектов
     * @param $images $_FILES
     * @return array
     */
    public function getImagesFormat($images);
}