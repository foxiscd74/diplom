<?php

namespace models;

use services\kernel\Kernel;
use DateTime;

/**
 * Class CarModels
 * Модель таблицы 'questionnaires'
 * @package models
 * @property int $modelId Марка авто
 * @property int $userId  Пользователь
 * @property float $price Цена
 * @property int $hP Лошадиные силы
 * @property string $dateOfIssue Дата выпуска
 * @property string $description Описание
 * @property string $createdAt дата создания
 */
class Questionnaires extends ActiveRecord
{
    protected $modelId;
    protected $userId;
    protected $price;
    protected $hP;
    protected $dateOfIssue;
    protected $description;
    protected $createdAt;

    /**
     * Возвращает название таблицы
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'questionnaires';
    }

    /**
     * Возвращает список правил
     * @return array[]
     */
    public function rules()
    {
        return [
            [['price', 'hP'], 'number'],
            [['dateOfIssue', 'description'], 'string'],
        ];
    }

    /**
     * @return string
     */
    public function getDateOfIssue(): string
    {
        $date = new DateTime($this->dateOfIssue);
        return $date->format('Y-m-d');
    }

    /**
     * @return int
     */
    public function getHP()
    {
        return $this->hP;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return CarModels|null
     */
    public function getModelId(): int
    {
        return $this->getModel()->getId();
    }

    /**
     * @return CarModels|null
     */
    public function getModel()
    {
        return CarModels::getById($this->modelId);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return User::getById($this->userId);
    }

    public function setUserId()
    {
        $this->userId = Kernel::getKernel()->user()->getUser()->id;
    }

    /**
     * @return CarImages[]|null
     */
    public function getImages()
    {
        return CarImages::findAll('WHERE `questionnaire_id`=' . $this->id);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (new DateTime($this->createdAt))->format('Y-m-d');
    }
}
