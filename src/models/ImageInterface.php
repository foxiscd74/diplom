<?php

namespace models;

/**
 * Interface ImageInterface
 * @package models
 */
interface ImageInterface
{
    /**
     * Возвращает путь к файлу из бд
     * @return string
     */
    public function getPicturePath();

    /**
     * Путь к директории файлов
     * @return string
     */
    public function getUploadPath();

    /**
     * Возвращает список расширений
     * @return string[]
     */
    public function getExtensions();
}