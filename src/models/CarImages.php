<?php

namespace models;

/**
 * Class CarImages
 * Модель таблицы 'car_images'
 * @package models
 * @property int $questionnaireId номер обьявления
 * @property string $image путь к файлу
 */
class CarImages extends ActiveRecord implements ImageInterface
{
    protected $questionnaireId;
    protected $image = 'q';

    /**
     * Возвращает название таблицы
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'car_images';
    }

    /**
     * Возвращает список правил
     * @return array[]
     */
    public function rules()
    {
        return [
            [['model'], 'safe'],
        ];
    }

    /**
     * Возвращает путь к файлу из бд
     * @return string
     */
    public function getPicturePath()
    {
        return $this->image;
    }

    /**
     * Путь к директории файлов
     * @return string
     */
    public function getUploadPath()
    {
        return '/src/web/uploads/CarImages';
    }

    /**
     * Возвращает список расширений
     * @return string[]
     */
    public function getExtensions()
    {
        return [
            'jpg',
            'jpeg',
            'png',
            'svg',
            'gif',
            'JPG'
        ];
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }
}