<?php

namespace models;

/**
 * Class CarModels
 * Модель таблицы 'car_models'
 * @package models
 *
 * @property int $model Марка авто
 */
class CarModels extends ActiveRecord
{
    protected $model;

    /**
     * Возвращает название таблицы
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'car_models';
    }

    /**
     * Возвращает список правил
     * @return array[]
     */
    public function rules()
    {
        return [
            [['model'], 'safe'],
        ];
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
}