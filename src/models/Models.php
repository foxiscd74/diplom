<?php

namespace models;

use exceptions\ValidateException;
use services\kernel\Kernel;

/**
 * Class Models
 * @package models
 * @property int $id
 */
abstract class Models
{

    /**
     * Получить объект как массив
     * @return array
     */
    public function getAsArray()
    {
        $vars = array_keys(get_class_vars(static::class));
        $arr = [];
        if (!empty($vars)) {
            foreach ($vars as $property) {
                $property = $this->underscoreToCamelCase($property);
                $arr[$property] = $this->$property;
            }
        }
        return $arr;
    }

    /**
     * Устанавливает значение свойства
     * @param string $name Название свойства
     * @param mixed $value Значение свойства
     */
    public function __set($name, $value)
    {
        $camelCaseName = $this->underscoreToCamelCase($name);
        $this->$camelCaseName = $value;
    }

    /**
     * Возвращает значение свойства
     * @param string $name Название свойства
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Возвращает правила валидации
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Возвращает результат проверки валидности модели
     * @return bool
     */
    public function validate()
    {
        try {
            if ($this->rules()) {
                foreach ($this->rules() as $rule) {
                    switch ($rule[1]) {
                        case 'required':
                            $method = 'isRequired';
                            break;
                        case 'string':
                            $method = 'isString';
                            break;
                        case 'number':
                            $method = 'isNumber';
                            break;
                        case 'safe':
                            $method = 'safe';
                            break;
                    }
                    if (is_array($rule[0])) {
                        foreach ($rule[0] as $rule) {
                            $this->$method($rule);
                        }
                    } else {
                        $this->$method($rule[0]);
                    }
                }
                return true;
            }
        } catch (ValidateException $exception) {
            return Kernel::getKernel()->session()->setSession('ErrorMessage', $exception->getMessage());
        }
    }

    /**
     * Проверка значения обязательного поля
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    private function isRequired($property)
    {
        if (is_array($this->$property)) {
            if (array_key_exists('tmp_name',$this->$property)) {
                if (empty($this->$property['tmp_name'])){
                    throw new ValidateException(
                        'Поле ' . $this->getAttributeLabel($property) . ' обязательно для заполнения'
                    );
                }
            }
        }
        if (!empty($this->$property)) {
            return true;
        } else {
            throw new ValidateException(
                'Поле ' . $this->getAttributeLabel($property) . ' обязательно для заполнения'
            );
        }
    }

    /**
     * Названия атрибутов
     * @return array
     */
    public function attributesLabels()
    {
        return [];
    }

    /**
     * Получить название атрибута
     * @param string $propertyStr название свойства
     * @return string
     */
    public function getAttributeLabel($property)
    {
        $labels = $this->attributesLabels();
        if (!empty($labels[$property])) {
            return '"' . $labels[$property] . '"';
        } else {
            return '"' . $property . '"';
        }
    }

    /**
     * Проверка значения свойства на строку
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    private function isString($property)
    {
        if (is_string($this->$property) && strlen($this->$property) > 0 && !is_numeric($this->$property)) {
            $this->$property = htmlentities($this->$property);
            return true;
        } else {
            throw new ValidateException(
                'Поле ' . $this->getAttributeLabel($property) . ' должно быть строкой'
            );
        }
    }

    /**
     * Проверка значения свойства на число
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    protected function isNumber($property)
    {
        if (is_numeric($this->$property)) {
            return true;
        } else {
            throw new ValidateException(
                'Поле ' . $this->getAttributeLabel($property) . ' должно быть числом'
            );
        }
    }

    /**
     * Безопасное
     * @param string $propertyStr названия свойства
     * @return bool
     */
    private function safe($property)
    {
        return true;
    }

    /**
     * Загрузка значений для атрибутов модели
     * @param array $request
     * @return bool
     */
    public function load($request)
    {
        if (!empty($request)) {
            $vars = array_keys((get_class_vars(static::class)));
            foreach ($request as $key => $value) {
                $key = $this->underscoreToCamelCase($key);
                if (in_array($key, $vars)) {
                    $this->$key = $value;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Метод преобразования under_score в CamelCase
     * @param string $string строка
     * @return string
     */
    protected function underscoreToCamelCase(string $string): string
    {
        return lcfirst(str_replace('_', '', ucwords($string, '_')));
    }

    /**
     * Метод преобразования CamelCase в under_score
     * @param string $string
     * @return string
     */
    protected function camelCaseToUnderscore($string)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));
    }
}