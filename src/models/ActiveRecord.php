<?php

namespace models;

use exceptions\DbException;
use services\DB;
use ReflectionProperty;
use ReflectionObject;

/**
 * Class ActiveRecord
 * @package models
 * @property int $id
 */
abstract class ActiveRecord extends Models
{
    protected $id;

    /**
     * Возвращает название таблицы
     * @return string
     */
    abstract protected static function getTableName(): string;


    /**
     * Поиск по значению колонки
     * @param string $columnName название колонки
     * @param string $value значение
     * @return mixed|null
     */
    public static function findOneByColumn(string $columnName, string $value)
    {
        $db = Db::getInstance();
        $result = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE `' . $columnName . '` = :value LIMIT 1;',
            [':value' => $value],
            static::class
        );
        if ($result === []) {
            return null;
        }
        return $result[0];
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Поиск по ID записи
     * @param int $id Static ID
     * @return static|null
     */
    public static function getById($id)
    {
        $db = Db::getInstance();
        $entities = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE id=:id;',
            [':id' => $id],
            static::class
        );

        return $entities ?
            $entities[0] :
            null;
    }

    /**
     * Найти все записи
     * @param string $string строка
     * @return array|null
     */
    public static function findAll(string $string = '')
    {
        $db = Db::getInstance();
        return $db->query('SELECT * FROM `' . static::getTableName() . '` ' . $string . ';', [], static::class);
    }


    /**
     * Метод сохранения модели
     */
    public function save(): int
    {
        $mappedProperties = $this->mapPropertiesToDbFormat();
        if ($this->id !== null) {
            $this->update($mappedProperties);
        } else {
            $this->insert($mappedProperties);
        }
        return $this->id;
    }

    /**
     * Метод изменения модели
     * @param array $mappedProperties сопоставленные свойства
     */
    private function update(array $mappedProperties)
    {
        $column2params = [];
        $params2values = [];
        $index = 1;
        foreach ($mappedProperties as $column => $value) {
            $param = ':param' . $index;
            $column2params[] = $column . ' = ' . $param;
            $params2values[$param] = $value;
            $index++;
        }
        $sql = 'UPDATE `' . static::getTableName() . '` SET ';
        $sql .= implode(', ', $column2params) . ' WHERE id= ' . $this->id;
        $db = DB::getInstance();
        $db->query($sql, $params2values, static::class);
    }

    /**
     * Метод добавления модели
     * @param array $mappedProperties сопоставленные свойства
     * @throws DbException
     */
    private function insert(array $mappedProperties)
    {
        $filterProperties = array_filter($mappedProperties);
        $columns = [];
        $params = [];
        $index = 1;
        foreach ($filterProperties as $column => $value) {
            $params[] = ':param' . $index;
            $columns[] = $column;
            $params2values[':param' . $index] = $value;
            $index++;
        }
        $sql = 'INSERT INTO `' . static::getTableName() . '` (';
        $sql .= implode(',', $columns) . ') VALUES (' . implode(',', $params) . ');';
        $db = DB::getInstance();
        $db->query($sql, $params2values, static::class);
        $this->id = $db->getLastInseartId();
        if ($this->id) {
            $this->refresh();
        } else {
            throw new DbException('Ошибка сохранения');
        }
    }

    /**
     * Метод сопоставления свойств формату базы данных
     * @return array
     */
    private function mapPropertiesToDbFormat(): array
    {
        $reflector = new ReflectionObject($this);
        $properties = $reflector->getProperties(ReflectionProperty::IS_PROTECTED);
        $mappedProperties = null;
        if (!empty($properties)) {
            foreach ($properties as $property) {
                $propertyName = $property->getName();
                $propertyNameAsUnderscore = $this->camelCaseToUnderscore($propertyName);
                $mappedProperties[$propertyNameAsUnderscore] = $this->$propertyName;
            }
        }
        return $mappedProperties;
    }

    /**
     * Метод обновления значений свойств модели
     */
    private function refresh()
    {
        $objectFromDb = static::getById($this->id);
        $reflector = new ReflectionObject($objectFromDb);
        $properties = $reflector->getProperties();
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $propertyName = $property->getName();
            $this->$propertyName = $property->getValue($objectFromDb);
        }
    }

    /**
     * Удаление
     * @return bool
     */
    public function delete(): bool
    {
        try {
            $db = DB::getInstance();
            $db->query('DELETE FROM `' . static::getTableName() . '` WHERE id = :id', [':id' => $this->id]);
            $this->id = null;
            return true;
        } catch (DbException $exception) {
            return false;
        }
    }
}