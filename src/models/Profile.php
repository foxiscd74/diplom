<?php

namespace models;

/**
 * Class User
 * Модель таблицы 'profiles'
 * @package models
 * @property User $user Юзер
 * @property int $userId ID Пользователя
 * @property string $firstName Имя
 * @property string $middleName Отчество
 * @property string $lastName Фамилия
 * @property string $birthDate Дата рождения
 * @property string $phone Телефон
 * @property string $avatar Фото
 * @property string $address Адрес
 */
class Profile extends ActiveRecord implements ImageInterface
{
    protected $userId;
    protected $firstName = '';
    protected $middleName = '';
    protected $lastName = '';
    protected $birthDate = '';
    protected $phone = '';
    protected $avatar = '/src/web/img/noimg.jpg';
    protected $address = '';
    public $user;

    /**
     * Возвращает название таблицы
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'profiles';
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return User::getById($this->userId);
    }

    /**
     * Возвращает список расширений
     * @return string[]
     */
    public function getExtensions()
    {
        return [
            'jpg',
            'jpeg',
            'png',
            'svg',
            'gif',
            'JPG'
        ];
    }

    /**
     * Возвращает путь к файлу из бд
     * @return string
     */
    public function getPicturePath()
    {
        return $this->avatar;
    }

    /**
     * Путь к директории файлов
     * @return string
     */
    public function getUploadPath()
    {
        return '/src/web/uploads/profiles';
    }
}