<?php

namespace models\forms;

use models\Models;
use models\Profile;
use services\kernel\Kernel;
use Exception;

/**
 * Class ProfileForm
 * Модель для формы 'profile->edit'
 * @package models\forms
 * @property string $email Емейл
 * @property string $phone Телефон
 * @property array $avatar Аватарка
 * @property string $address Адрес
 * @property string $lastName Фамилия
 * @property string $firstName Имя
 * @property string $middleName Отчество
 * @property string $birthDate Дата рождения
 */
class ProfileForm extends Models
{
    protected $email;
    protected $phone;
    protected $avatar;
    protected $address;
    protected $lastName;
    protected $firstName;
    protected $middleName;
    protected $birthDate;

    /**
     * Возвращает список правил
     * @return array[]
     */
    public function rules()
    {
        return [
            [
                ['email', 'firstName', 'lastName', 'birthDate', 'phone'],
                'required'
            ],
            [
                ['address', 'middleName', 'avatar'],
                'safe'
            ]
        ];
    }

    public function attributesLabels()
    {
        return [
            'email'=>'Email',
            'firstName' => 'Имя',
            'lastName' => 'Фамилия',
            'birthDate' => 'Дата рождения',
            'phone' => 'Телефон',
        ];
    }

    /**
     * Сохранение модели в БД
     * @param Profile $profile Профиль
     */
    public function save(Profile $profile)
    {
        try {
            $user = Kernel::getKernel()->user()->getUser();
            $user->email = $this->email;
            $profile->firstName = $this->firstName;
            $profile->lastName = $this->lastName;
            $profile->middleName = $this->middleName;
            $profile->address = $this->address;
            $profile->phone = $this->phone;
            $profile->birthDate = $this->birthDate;
            $profile->userId = Kernel::getKernel()->user()->getUser()->getId();
            $url = Kernel::getKernel()->getOperations()->saveFile($this->avatar, $profile);
            $profile->avatar = $url;
            $profile->save();
        } catch (Exception $exception) {
            Kernel::getKernel()->session()->setSession('ErrorMessage', $exception->getMessage());
        }
    }
}