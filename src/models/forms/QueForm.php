<?php

namespace models\forms;

use models\CarImages;
use models\Models;
use models\Questionnaires;
use services\kernel\Kernel;
use exceptions\DbException;
use Exception;

/**
 * Class QueForm
 * Модель для формы 'add_que'
 * @package models
 * @property array $images Картинки
 * @property int $modelId Ид модели
 * @property float $price Цена
 * @property int $hP Лошадиные силы
 * @property string $dateOfIssue Дата выпуска
 * @property string $description описание
 */
class QueForm extends Models
{
    protected $modelId;
    protected $price;
    protected $hP;
    protected $dateOfIssue;
    protected $description;
    public $images;

    /**
     * Возвращает список правил
     * @return array[]
     */
    public function rules()
    {
        return [
            [['modelId', 'dateOfIssue'], 'required'],
            [['dateOfIssue', 'description'], 'string'],
            [['price', 'hP'], 'number'],
            [['images'], 'safe'],
        ];
    }

    public function attributesLabels()
    {
        return [
            'dateOfIssue' => 'Дата выпуска',
            'price' => 'Цена',
            'hP' => 'Лошадиные силы',
            'description' => 'Описание',
            'images' => 'Изображение',
        ];
    }

    /**
     *  Сохранение модели в БД
     * @param int $id ID анкеты
     * @throws DbException
     */
    public function save($id = null)
    {
        try {
            if ($id == null) {
                $questionnaire = new Questionnaires();
            } else {
                $questionnaire = Questionnaires::getById($id);
            }
            if ($questionnaire) {
                $questionnaire->dateOfIssue = $this->dateOfIssue;
                $questionnaire->price = $this->price;
                $questionnaire->modelId = $this->modelId;
                $questionnaire->hP = $this->hP;
                $questionnaire->description = $this->description;
                $questionnaire->createdAt = date('Y-m-d H:i:s');
                $questionnaire->setUserId();
                $id = $questionnaire->save();
                if ($id) {
                    foreach ($this->images as $image) {
                        if (!empty($image['tmp_name'])) {
                            $carImage = new CarImages();
                            $url = Kernel::getKernel()->getOperations()->saveFile($image, $carImage);
                            if ($url) {
                                $carImage->image = $url;
                            }
                            $carImage->questionnaireId = $questionnaire->getId();
                            $carImage->save();
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            if ($questionnaire->getId() && !$id) {
                $questionnaire->delete();
            }
            Kernel::getKernel()->session()->setSession('ErrorMessage', $exception->getMessage());
        }
    }
}