<?php

namespace models;

use services\kernel\Kernel;
use Exception;

/**
 * Class User
 * Модель таблицы 'users'
 * @package models
 * @property string $nickname Логин пользователя
 * @property string $email Email пользователя
 * @property string $passwordHash Хэш пароля
 * @property string $authToken Токен авторизации
 * @property string $createdAt Дата создания
 * @property string $password Пароль
 */
class User extends ActiveRecord
{
    protected $nickname;
    protected $email;
    protected $passwordHash;
    protected $authToken;
    protected $createdAt;
    public $password;

    /**
     * Возвращает название таблицы
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'users';
    }

    /**
     * Возвращает правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['nickname', 'password', 'email'], 'required'],
            [['passwordHash', 'authToken', 'createdAt'], 'safe'],
        ];
    }

    public function attributesLabels()
    {
        return [
            'nickname' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Email',
        ];
    }

    /**
     * Метод регистрации пользователя
     * @return $this|false
     * @throws Exception
     */
    public function signUp()
    {
        if (!User::findOneByColumn('email', $this->email)) {
            $this->passwordHash = password_hash($this->password, PASSWORD_DEFAULT);
            $this->authToken = sha1(random_bytes(100)) . sha1(random_bytes(100));
            $this->createdAt = date('Y-m-d');
            $this->save();
            return $this;
        }
        Kernel::getKernel()->session()->setSession('ErrorMessage', 'Такой пользователь существует');
        return false;
    }

    /**
     * Метод обновления токена
     * @throws Exception
     */
    private function refreshAuthToken()
    {
        $this->authToken = sha1(random_bytes(100)) . sha1(random_bytes(100));
        $this->save();
    }

    /**
     * Метод авторизации пользователя
     * @param $email
     * @param $password
     * @return mixed
     */
    public static function login($email, $password)
    {
        $user = User::findOneByColumn('email', $email);
        if ($user == null || !password_verify($password, $user->getPasswordHash())) {
            return Kernel::getKernel()->session()->setSession('ErrorMessage', 'Логин или пароль не верные');
        }
        $user->refreshAuthToken();
        Kernel::getKernel()->user()->login($user);
        return $user;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * Получить профиль пользователя
     * @return Profile|null
     */
    public function getProfile()
    {
        return Profile::findOneByColumn('user_id', $this->id);
    }
}