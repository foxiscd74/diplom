<?php

namespace controllers;

use models\CarModels;
use models\Questionnaires;
use services\Pagination;
use DateTime;

/**
 * Class MainController
 * @package controllers
 */
class MainController extends AbstractController
{
    /**
     * Главная страница
     */
    public function actionIndex()
    {
        $this->view->setTitle('Главная страница');
        $carModels = CarModels::findAll();
        if (empty($_GET['filter'])) {
            $questionnaires = Questionnaires::findAll('ORDER BY created_at desc');
        } elseif (isset($_GET['filter'])) {
            // фильтр
            $arr = [];
            $dateFrom = "'" . (new DateTime($_GET['from_date_issue']))->format('Y-m-d H:i:s') . "'";
            $dateTo = "'" . (new DateTime($_GET['to_date_issue']))->format('Y-m-d H:i:s') . "'";
            $arr[] = $_GET['from_date_issue'] ? ' date_of_issue >= ' . $dateFrom . ' ' : null;
            $arr[] = $_GET['to_date_issue'] ? ' date_of_issue <= ' . $dateTo . ' ' : null;
            $arr[] = $_GET['from_price'] ? ' price >= ' . $_GET['from_price'] . ' ' : null;
            $arr[] = $_GET['to_price'] ? ' price <= ' . $_GET['to_price'] . ' ' : null;
            $arr[] = $_GET['from_HP'] ? ' h_p >= ' . $_GET['from_HP'] . ' ' : null;
            $arr[] = $_GET['to_HP'] ? ' h_p <= ' . $_GET['to_HP'] . ' ' : null;
            $arr[] = $_GET['model'] ? ' model_id = ' . $_GET['model'] . ' ' : null;
            $sql = null;
            foreach ($arr as $value) {
                if ($value) {
                    if ($sql) {
                        $sql .= ' AND ' . $value;
                    } else {
                        $sql = ' WHERE' . $value;
                    }
                }
            }
            $questionnaires = Questionnaires::findAll(
                $sql
                . 'ORDER BY created_at desc'
            );
        }
        $page = (!empty($_GET['page'])) ? $_GET['page'] : null;
        $pagination = new Pagination($questionnaires, 3, $page);
        $questionnaires = $pagination->getPageContent();
        $count = $pagination->getCountPage();
        return $this->view->render('index', [
            'questionnaires' => $questionnaires,
            'count' => $count,
            'carModels' => $carModels
        ]);
    }
}