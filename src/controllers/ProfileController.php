<?php

namespace controllers;

use models\forms\ProfileForm;
use models\Profile;
use models\Questionnaires;
use models\User;
use services\kernel\Kernel as K;

/**
 * Class ProfileController
 * @package controllers
 */
class ProfileController extends AbstractController
{
    /**
     * Профиль пользователя
     * @param int $id ИД профиля
     * @return string
     */
    public function actionProfile($id)
    {
        $user = User::getById($id);
        $this->view->setTitle('Профиль ' . $user->getNickname());
        $checkUser = false;
        if (
            K::getKernel()->user()->getUser() && $user->getId() == K::getKernel()->user()->getUser()->getId()
        ) {
            $checkUser = true;
        }
        $questionnaires = Questionnaires::findAll(' WHERE user_id=' . $id);
        if (empty($profile = $user->getProfile())) {
            $profile = new Profile();
        }
        return $this->view->render(
            'profile',
            ['questionnaires' => $questionnaires, 'user' => $user, 'profile' => $profile, 'checkUser' => $checkUser]
        );
    }

    /**
     * Изменение профиля
     * @param int $id ИД профиля
     * @return string|void
     */
    public function actionProfileEdit($id)
    {
        $user = User::getById($id);
        if ($user && $user->getId() == K::getKernel()->user()->getUser()->getId()) {
            $post = $_POST;
            if (empty($profile = $user->getProfile())) {
                $profile = new Profile();
            }
            $model = new ProfileForm();
            if ($model->load($post) && $model->validate()) {
                $model->avatar = K::getKernel()->getOperations()->getImagesFormat($_FILES['avatar']);
                $model->save($profile);
                $this->view->redirect('/');
            } else {
                $model->email = $user->getEmail();
                $model->firstName = $profile->getFirstName();
                $model->lastName = $profile->getLastName();
                $model->middleName = $profile->getMiddleName();
                $model->address = $profile->getAddress();
                $model->phone = $profile->getPhone();
                $model->avatar = $profile->getAvatar();
                $model->birthDate = $profile->getBirthDate();
                $this->view->setTitle('Изменить профиль ' . $user->getNickname());
                return $this->view->render('edit', ['model' => $model]);
            }
        }
        $this->view->redirect('/');
    }
}