<?php

namespace controllers;

use models\CarImages;
use models\CarModels;
use models\forms\QueForm;
use models\Questionnaires;
use services\kernel\Kernel;
use exceptions\DbException;

/**
 * Class QuestionnairesController
 * @package controllers
 */
class QuestionnairesController extends AbstractController
{
    /**
     * Добавление объявления
     * @return string|void
     * @throws DbException
     */
    public function actionInsert()
    {
        if (!empty(Kernel::getKernel()->user()->getUser())) {
            if (!empty($_POST)) {
                $model = new QueForm();
                if ($model->load($_POST) && $model->validate()) {
                    $model->images = Kernel::getKernel()->getOperations()->getImagesFormat($_FILES['images']);
                    $model->save();
                }
            }
            $this->view->setTitle('Добавление объявления');
            $carModels = CarModels::findAll();
            return $this->view->render('add_que', ['models' => $carModels]);
        }
        Kernel::getKernel()->session()->setSession('ErrorMessage', 'Необходимо авторизоваться');
        $this->view->redirect('/');
    }

    /**
     * Удаление объявления
     * @param int $id Ид анкеты
     * @throws DbException
     */
    public function actionDelete($id)
    {
        $que = Questionnaires::getById($id);
        if (!empty($user = Kernel::getKernel()->user()->getUser()) && $user->getId() == $que->getUser()->getId()) {
            $images = $que->getImages();
            foreach ($images as $image) {
                if ($image->image != Kernel::getAlias('noImg')) {
                    Kernel::getKernel()->getOperations()->deleteFile($image);
                }
                $image->delete();
            }
            $que->delete();
            $this->view->redirect('/profile/' . Kernel::getKernel()->user()->getUser()->getId());
        }
        Kernel::getKernel()->session()->setSession('ErrorMessage', 'Недостаточно прав');
        $this->view->redirect('/');
    }

    /**
     * Изменение объявления
     * @param int $id Ид анкеты
     * @return string|void
     * @throws DbException
     */
    public function actionEdit($id)
    {
        $que = Questionnaires::getById($id);
        $model = new QueForm();
        if (!empty($user = Kernel::getKernel()->user()->getUser()) && $user->getId() == $que->getUser()->getId()) {
            if (!empty($_POST) && $model->load($_POST) && $model->validate()) {
                $model->images = Kernel::getKernel()->getOperations()->getImagesFormat($_FILES['images']);
                $model->save($id);
            }
            $model->load($que->getAsArray());
            $carModels = CarModels::findAll();
            $this->view->setTitle('Изменение объявления');
            return $this->view->render(
                'edit',
                ['model' => $model, 'carModels' => $carModels, 'questionnaire' => $que]
            );
        }
        Kernel::getKernel()->session()->setSession('ErrorMessage', 'Недостаточно прав');
        $this->view->redirect('/');
    }

    /**
     * Удаление фотографии
     * @return string
     * @return false|string
     * @throws DbException
     */
    public function deleteImage()
    {
        if (!empty($id = $_POST['id'])) {
            $image = CarImages::getById($id);
            if ($image && $image->delete()) {
                return $this->asJson(true);
            }
        }
        return $this->asJson(false);
    }
}