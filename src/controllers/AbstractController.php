<?php

namespace controllers;

use services\View;

/**
 * Class AbstractController
 * @package controllers
 */
abstract class AbstractController
{
    /**
     * @var View $view Экземпляр view
     */
    protected $view;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $controllerName = $this->parseControllerName();
        $this->view = new View(DIR_APP . 'views/' . $controllerName);
    }

    /**
     * Парсит название контроллера
     * @return string
     */
    private function parseControllerName()
    {
        preg_match('`\\\(.*)$`', static::class, $matches);
        return strtolower(str_ireplace('Controller', '', $matches[1]));
    }

    /**
     * Передает ответ в формате json
     * @param mixed $value значение
     * @return false|string
     */
    public function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}