<?php

namespace controllers;

use models\User;
use services\kernel\Kernel;
use Exception;

/**
 * Контроллер обработки пользователя
 */
class UserController extends AbstractController
{
    /**
     * Регистрация пользователя
     * @return string
     * @throws Exception
     */
    public function actionRegister()
    {
        $this->view->setTitle('Регистрация');
        if (!empty($_POST)) {
            $user = new User();
            if ($user->load($_POST) && $user->validate() && $user->signUp()) {
                Kernel::getKernel()->user()->login($user);
                $this->view->redirect('/');
            }
        }
        return $this->view->render('sign_up');
    }

    /**
     * Логин пользователя
     * @return string
     */
    public function actionLogin()
    {
        $this->view->setTitle('Авторизация');
        if (!empty($_POST)) {
            if (User::login($_POST['email'], $_POST['password'])) {
                $this->view->redirect('/');
            }
        }
        return $this->view->render('sign_in');
    }

    /**
     * Выход из профиля
     */
    public function actionLogout()
    {
        Kernel::getKernel()->user()->logout();
        $this->view->redirect('/');
    }
}