<?php

return [

    //Настройки базы данных
    'db' => include 'db_options.php',

    //Список операций проекта
    'operations' => [
        \operations\UsersAuthService::class,
        \operations\FileHandler::class,
    ],

    //Алиасы проекта
    'aliases' => [
        '@domain' => 'http://diplom.ru',
        '@path' => $_SERVER['DOCUMENT_ROOT'],
        'noImg' => '/src/web/img/noimg.jpg',
        'logo' => '/src/web/img/logo.png',
    ]
];