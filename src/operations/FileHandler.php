<?php

namespace operations;

use exceptions\FileHandlerException;
use models\ImageInterface;
use services\kernel\Kernel;

/**
 * Class FileHandler
 * Операции для работы с файлами
 * @package app\components
 * @property ImageInterface $model
 * @property array $file
 */
class FileHandler
{
    private $model;
    private $file;

    /**
     * Метод сохранения файла
     * @param array $file
     * @param ImageInterface $model
     */
    public function saveFile($file, ImageInterface $model)
    {
        try {
            if (strlen($file['name']) == 0) {
                $url = Kernel::getAlias('noImg');
                return $url;
            }
            $this->model = $model;
            $this->file = $file;
            $url = $model->getUploadPath() . '/';
            if (!is_dir(Kernel::getAlias('@path') . $url)) {
                mkdir(Kernel::getAlias('@path') . $url, 077);
            }
            $url .= Kernel::getKernel()->user()->getUser()->getId() . '/';
            if (!is_dir(Kernel::getAlias('@path') . $url)) {
                mkdir(Kernel::getAlias('@path') . $url, 077);
            }

            $url .= $file['name'];
            $this->saveAs($url);
        } catch (FileHandlerException $exception) {
            throw new \Exception($exception->getMessage());
        }
        return $url;
    }

    /**
     * Созхранить как
     * @param string $url
     * @param array $file
     */
    private function saveAs($url)
    {
        $newFilePath = Kernel::getAlias('@path') . $url;
        $allowedExtention = $this->model->getExtensions();
        $extention = pathinfo($this->file['name'], PATHINFO_EXTENSION);
        preg_match("/[А-Яа-я]/", $this->file['name'], $pattern);
        if (!empty($pattern)) {
            throw new FileHandlerException('Файл должен иметь название латинского алфавита');
        }
        if (!in_array($extention, $allowedExtention)) {
            throw new FileHandlerException('Запрещенный формат файла');
        }
        if (file_exists($newFilePath)) {
            throw new FileHandlerException('Файл с именем ' . $this->file['name'] . ' существует');
        }
        if (!move_uploaded_file($this->file['tmp_name'], $newFilePath)) {
            throw new FileHandlerException('Ошибка загрузки файла');
        }
    }

    /**
     * Изменить файл
     * @param array $file
     * @param ImageInterface $model
     */
    public function updateFile($file, ImageInterface $model)
    {
        if ($this->deleteFile($model)) {
            return $this->saveFile($file, $model);
        }
    }


    /**
     * Удалить файл
     * @param array $file
     * @param ImageInterface $model
     */
    public function deleteFile(ImageInterface $model)
    {
        if ($model->getPicturePath()) {
            return unlink(Kernel::getAlias('@path') . $model->getPicturePath());
        }
    }


    /**
     * Получить картинки в формате для работы с изменением и сохранением
     * @param $images $_FILES
     * @return array
     */
    public function getImagesFormat($images)
    {
        $arrImages = [];
        foreach ($images as $name => $type) {
            if (is_array($type)) {
                foreach ($type as $key => $value) {
                    $arrImages[$key][$name] = $value;
                }
                continue;
            }
            return $images;
        }
        return $arrImages;
    }
}