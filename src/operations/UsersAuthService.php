<?php

namespace operations;

use models\User;
use services\kernel\AbstractKernelOperation;

/**
 * Class UsersAuthService
 * Клас для работы с авторизацие пользователя
 * @package operations
 */
class UsersAuthService extends AbstractKernelOperation
{
    /**
     * Создать токен
     * @param User $user
     */
    public function createToken(User $user)
    {
        $token = $user->getId() . ':' . $user->getAuthToken();
        setcookie('token', $token, 0, '/');
    }

    /**
     * Удалить токен
     */
    public function deleteToken()
    {
        setcookie('token', '', time() - 10, '/');
    }

    /**
     * Получить пользователя по токену
     * @return User|null
     */
    public function getUserByToken()
    {
        $token = $_COOKIE['token'] ?? '';
        if (empty($token)) {
            return null;
        }
        $logUser = explode(':', $token, 2);
        $userId = $logUser[0];
        $authToken = $logUser[1];
        $user = User::getById($userId);
        if ($user === null) {
            return null;
        }
        if ($user->getAuthToken() !== $authToken) {
            return null;
        }
        return $user;
    }
}