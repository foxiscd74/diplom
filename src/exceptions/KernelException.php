<?php

namespace exceptions;

use Exception;

/**
 * Class KernelException
 * Выбрасывается в ядре
 * @package exceptions
 */
class KernelException extends Exception
{
}