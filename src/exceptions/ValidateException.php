<?php

namespace exceptions;

use Exception;

/**
 * Class ValidateException
 * Выбрасывается при валидации данных
 * @package exceptions
 */
class ValidateException extends Exception
{
}