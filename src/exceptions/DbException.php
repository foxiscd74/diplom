<?php

namespace exceptions;

use Exception;

/**
 * Class DbException
 * Выбрасывается при ошибках обращения к бд
 * @package exceptions
 */
class DbException extends Exception
{
}