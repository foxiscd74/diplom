<?php

namespace exceptions;

use Exception;

/**
 * Class FileHandlerException
 * Выбрасывается при ошибках сохранения файла
 * @package exceptions
 */
class FileHandlerException extends Exception
{
}