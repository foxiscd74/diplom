// инициализация слайдера
$('.single-item').slick({
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    adaptiveHeight: true
});

// открытие закрытие фильтра
function toggleFilter(elem) {
    var filter = elem.parentElement;
    var name = elem.querySelector('.filter_name');
    if (filter.classList.contains('active')) {
        filter.classList.remove('active')
        name.innerHTML = 'Ф И Л Ь Т Р';
    } else {
        filter.classList.add('active');
        name.innerHTML = 'С К Р Ы Т Ь';
    }
}

// перенаправить
function redirect(string) {
    window.location = string;
}

// проверка на удаление
function checkActionDelete(string) {
    var y = confirm('Вы действительно хотите удалить запись?');
    if (y) {
        redirect(string);
    }
}

// отображение мини меню
function menuImg(block) {
    var buttonDelete = $(block).closest('div').find('div');
    if (buttonDelete.hasClass('active')) {
        buttonDelete.removeClass('active')
    } else {
        buttonDelete.addClass('active');
    }
}

// удаление картинки
function deleteImage(block) {
    var id = block.closest('.item-picture').getAttribute('data-img');
    $.ajax({
        url: '/deleteImage',
        method: 'post',
        data: {id: id},
        success: function (data) {
            if (data) {
                block.closest('.item-picture').remove();
            }
        }
    });
}

function clearForm(button) {
    var form = button.closest('form');
    $(form).find('input').prop('value',null);
}