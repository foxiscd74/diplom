<?php
return [
    '`^register$`' => ['controllers\UserController', 'actionRegister'],                   // Регистрация
    '`^login$`' => ['controllers\UserController', 'actionLogin'],                         // Вход в профиль
    '`^logout$`' => ['controllers\UserController', 'actionLogout'],                       // Выход из профиля
    '`^profile/(\d+)$`' => ['controllers\ProfileController', 'actionProfile'],            // Просмотр профиля
    '`^profile/(\d+)/edit$`' => ['controllers\ProfileController', 'actionProfileEdit'],   // Изменение профиля
    '`^add_que$`' => ['controllers\QuestionnairesController', 'actionInsert'],            // Добавление объявления
    '`^que/(\d+)/edit$`' => ['controllers\QuestionnairesController', 'actionEdit'],       // Изменение объявления
    '`^que/(\d+)/delete$`' => ['controllers\QuestionnairesController', 'actionDelete'],   // Удаление объявления
    '`^/$`' => ['controllers\MainController', 'actionIndex'],                             // Главная страница
    '`^/deleteImage$`' => ['controllers\QuestionnairesController', 'deleteImage'],        // Удаление картинки
];